﻿using System;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.CLI.DTO;

namespace CoolParking.CLI
{
    /// <summary>
    /// Main class of command line interface for client
    /// </summary>
    class Program
    {
        // Instanse of service that requests to API
        static readonly IClientService _clientService;

        // URL of localhost
        static readonly string _localhostUrl = "http://localhost:62539/";

        /// <summary>
        /// Base constructor
        /// </summary>
        static Program()
        {
            _clientService = new ClientService(_localhostUrl);
        }

        /// <summary>
        /// Main method - start point of programm
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Call Program contstructor to create instance of ParkingService
            new Program();

            // String for user commands 
            string command = String.Empty;

            // Program menu to run commands
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("> Choose command");

                Console.WriteLine("\ta - Park a new vehicle");
                Console.WriteLine("\tb - Display parking balance");
                Console.WriteLine("\tc - Display free and busy places");
                Console.WriteLine("\th - Display transaction history");
                Console.WriteLine("\ti - Display current income");
                Console.WriteLine("\tp - Pick up vehicle from parking");
                Console.WriteLine("\tt - Display all transactions for current period");
                Console.WriteLine("\tu - Top up vehicle balance");
                Console.WriteLine("\tv - Display all vehicles at parking");
                Console.WriteLine("\tx - Exit");

                // Read user's command
                command = Console.ReadLine();
                
                // Check if user want's to exit
                if (command != "x")
                {
                    Console.ForegroundColor = ConsoleColor.White;

                    // Choose and execute command
                    ExecuteCommand(command);
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Choose and execute command that user entred
        /// </summary>
        /// <param name="command"></param>
        static void ExecuteCommand(string command)
        {
            try
            {
                // Chose and execute command
                switch (command)
                {
                    case "a": AddVehicle(); break;
                    case "b": DisplayParkingBalance(); break;
                    case "c": DisplayParkingBusiness(); break;
                    case "h": DisplayTransactionHistory(); break;
                    case "i": DisplayParkingIncome(); break;
                    case "p": PickUpVehicle(); break;
                    case "t": DisplayLastTransactions(); break;
                    case "u": TopUpVehicleBalance(); break;
                    case "v": DisplayAllVehicles(); break;

                        //If command is not found make warning
                    default: MakeWarning("Incorrect command was entred!"); break;
                }

                Console.WriteLine("> Press any key to continue...");
                Console.ReadKey();
            }

            // Exception hadling
            catch (InvalidOperationException e)
            {
                HandleException(e, command);
            }
            catch (ArgumentException e)
            {
                HandleException(e, command);
            }
            catch (Exception e)
            {
                HandleException(e, command);
            }
        }

        /// <summary>
        /// Handle exceptions with warning messages in consol
        /// </summary>
        /// <param name="e"></param>
        /// <param name="commandToRepeat"></param>
        static void HandleException(Exception e, string commandToRepeat)
        {
            // Make warning about exception
            MakeWarning(e.Message);
            
            // Ask if user want's to try again
            if (AskToRepeat())
            {
                // Execute the same command one more time
                ExecuteCommand(commandToRepeat);
            }
        }

        /// <summary>
        /// Display all vehicles
        /// </summary>
        static async void DisplayAllVehicles()
        {
            // Get all vehicles and wait for result
            var task = _clientService.GetVehicles();
            task.Wait();
            IList<VehicleDTO> vehicles = task.Result;

            // Check if there is at least one vehicle at parking
            if (vehicles.Count > 0)
            {
                Console.WriteLine("\t>> Vehicles at parking");
                Console.WriteLine("Vehicle Id\tType\tBalance");
                
                // Display list of all vehicles at parking
                foreach(var vehicle in vehicles)
                {
                    Console.WriteLine($"{vehicle.Id}\t{Enum.GetName(typeof(VehicleTypeDTO), vehicle.VehicleType)}\t{vehicle.Balance}");
                }
            }
            else
            {
                MakeWarning("There is no vehicle at parking!");
            }
        }

        /// <summary>
        /// Top up vehicle balance by id
        /// </summary>
        static void TopUpVehicleBalance()
        {
            // Request vehicle id
            string vehicleId = GetInput("Enter 'Id'");

            // Request sum to top up and try to parse it to decimal
            decimal sum = Decimal.Parse(GetInput("Enter sum to top up"));

            // Top up balance
            _clientService.TopUpVehicle(vehicleId, sum);

            InformAboutSuccess($"Vehicle '{vehicleId}' balance was topped up!");
        }

        /// <summary>
        /// Display new transaction for the last time before writing to log
        /// </summary>
        static async void DisplayLastTransactions()
        {
            Console.WriteLine("\t>> Last parking transactions");

            // Get all new transaction for the last time and wait for result
            var task = _clientService.GetLastParkingTransactions();
            task.Wait();
            TransactionInfoDTO[] lastParkingTransactions = task.Result;

            // Check if there is at least one transaction
            if (lastParkingTransactions.Length != 0)
            {
                Console.WriteLine("Date/Time\t\tVehicle Id\tTax");

                // Display all transactions
                foreach (var trans in lastParkingTransactions)
                {
                    Console.WriteLine(trans.ToString());
                }
            }
            else
            {
                MakeWarning("There is no transaction for the last time!");
            }
        }

        /// <summary>
        /// Remove vehicle from parking by id
        /// </summary>
        static void PickUpVehicle()
        {
            // Request vehicle if
            var vehicleId = GetInput("Enter 'Id'");

            // Remove vehicle from parking
            _clientService.RemoveVehicle(vehicleId);

            InformAboutSuccess($"Vehicle '{vehicleId}' was picked up!");
        }

        /// <summary>
        /// Display parking income due to new transaction before writing to log
        /// </summary>
        static async void DisplayParkingIncome()
        {
            // Get sum of last transaction and wait for result
            var task = _clientService.GetLastParkingTransactions();
            task.Wait();
            TransactionInfoDTO[] lastTransactions = task.Result;

            // Calculate sum of last transactions
            var sum = lastTransactions.Sum(t => t.Sum);
            Console.WriteLine($"\t>> Parking income: {sum}");
        }

        /// <summary>
        /// Display free and busy places at parking at the moment
        /// </summary>
        static async void DisplayParkingBusiness()
        {
            // Get free places and wait for result
            var task = _clientService.GetFreePlaces();
            task.Wait();
            var freePlaces = task.Result;

            // Calculate busy places
            task = _clientService.GetCapacity();
            task.Wait();
            var busyPlaces = task.Result - freePlaces;

            Console.WriteLine($"\t>> Free places: {freePlaces}\n\t>> Busy places: {busyPlaces}");
        }

        /// <summary>
        /// Display all transaction from log file
        /// </summary>
        static async void DisplayTransactionHistory()
        {
            // Get all transation from log file and wait for result
            var task = _clientService.GetLogTransactions();
            task.Wait();
            string logHistory = task.Result;
            
            // Check if log file is created but empty
            if (logHistory != String.Empty)
            {
                Console.WriteLine("\t>> Transaction log history");
                Console.WriteLine(logHistory);
            }
            else
            {
                MakeWarning("Transaction log history is empty!");
            }
        }

        /// <summary>
        /// Add new vehicle to parking
        /// </summary>
        static void AddVehicle()
        {
            Console.WriteLine("\n> Enter data of new vehicle\n");

            // Request vehicle id
            string id = GetInput("Enter 'Id'");

            Console.WriteLine("\t>> Vehicle types: 0 - car, 1 - truck, 2 - bus, 3 - motorbike");
            
            // Request vehicle type
            VehicleTypeDTO type = (VehicleTypeDTO)Int32.Parse(GetInput("Enter 'Type'"));

            // Request vehicle balance
            decimal balance = Decimal.Parse(GetInput("Enter 'Balance'"));
            
            // Add new vehicle to parking
            _clientService.AddVehicle(new VehicleDTO(id, type, balance));

            InformAboutSuccess($"Vehicle with 'Id' {id} was added successfully!");
        }

        /// <summary>
        /// Ask user to try execute command one more time
        /// </summary>
        /// <returns></returns>
        static bool AskToRepeat()
        {
            while (true)
            {
                // Request approving to try one more time or exit
                Console.WriteLine("> Try once more? y / n");
                string command = Console.ReadLine();

                // Check if answer 'yes' then try again, if 'no' back to menu, else ask one more time
                if (command == "y")
                {
                    return true;
                }
                else if (command == "n")
                {
                    return false; ;
                }
                else
                {
                    MakeWarning("> Incorrect type of command!");
                    continue;
                }
            }
        }

        /// <summary>
        /// Display information message that operation finished successfully
        /// </summary>
        /// <param name="message"></param>
        static void InformAboutSuccess(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"> Success! {message}\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Display parking balance
        /// </summary>
        static async void DisplayParkingBalance()
        {
            // Get parking balance and wait for result
            var task = _clientService.GetBalance();
            task.Wait();
            var balance = task.Result;

            Console.WriteLine($"\t>> Parking balance: {balance}");
        }

        /// <summary>
        /// Display warning messages
        /// </summary>
        /// <param name="message"></param>
        static void MakeWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"> Warning! {message}\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Request user to enter some data
        /// </summary>
        /// <param name="str"></param>
        /// <returns>User input</returns>
        static string GetInput(string str)
        {
            Console.Write($"\t>> {str}: ");
            Console.ForegroundColor = ConsoleColor.White;
            var input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            return input;
        }
    }
}

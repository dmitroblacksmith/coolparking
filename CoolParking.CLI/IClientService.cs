﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoolParking.CLI.DTO;
using CoolParking.BL.Models;

namespace CoolParking.CLI
{
    public interface IClientService
    {
        public Task<IList<VehicleDTO>> GetVehicles();

        public Task<VehicleDTO> GetVehicleById(string id);

        public Task<decimal> GetBalance();

        public Task<int> GetCapacity();

        public Task<int> GetFreePlaces();

        public void AddVehicle(VehicleDTO vehicle);

        public void RemoveVehicle(string id);

        public void TopUpVehicle(string id, decimal sum);

        public Task<TransactionInfoDTO[]> GetLastParkingTransactions();

        public Task<string> GetLogTransactions();
    }
}

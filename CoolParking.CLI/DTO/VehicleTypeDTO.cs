﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.CLI.DTO
{
    /// <summary>
    /// DTO enum for type of vehicle
    /// </summary>
    public enum VehicleTypeDTO
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}

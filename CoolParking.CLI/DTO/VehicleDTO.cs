﻿using Newtonsoft.Json;

namespace CoolParking.CLI.DTO
{
    /// <summary>
    /// DTO entity for transfering vehicle data
    /// </summary>
    public class VehicleDTO
    {
        /// <summary>
        /// Id of vehicle
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Type of vehicle
        /// </summary>
        [JsonProperty("vehicleType")]
        public VehicleTypeDTO VehicleType { get; set; }

        /// <summary>
        /// Vehicle balance
        /// </summary>
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        [JsonConstructor]
        public VehicleDTO() { }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vehicleType"></param>
        /// <param name="balance"></param>
        public VehicleDTO(string id, VehicleTypeDTO vehicleType, decimal balance)
        {
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }
    }
}

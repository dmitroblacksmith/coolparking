﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CoolParking.CLI.DTO
{
    /// <summary>
    /// DTO entity for transfering transaction data
    /// </summary>
    public struct TransactionInfoDTO
    {
        /// <summary>
        /// Id of vehicle which the fee was charged from
        /// </summary>
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        /// <summary>
        /// Fee that was charged from vehicle
        /// </summary>
        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        /// <summary>
        /// Date and time of transaction
        /// </summary>
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="transactionTime"></param>
        /// <param name="vehicleId"></param>
        /// <param name="sum"></param>
        public TransactionInfoDTO(DateTime transactionDate, string vehicleId, decimal sum)
        {
            this.TransactionDate = transactionDate;
            this.VehicleId = vehicleId;
            this.Sum = sum;
        }

        /// <summary>
        /// Overrode version of base ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{TransactionDate}\t{VehicleId}\t{Sum}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Globalization;
using CoolParking.BL.Models;
using CoolParking.CLI.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CoolParking.CLI
{
    /// <summary>
    /// Service for sending requests to API
    /// </summary>
    class ClientService : IClientService
    {
        /// <summary>
        /// Instance of http client
        /// </summary>
        private HttpClient _client;

        /// <summary>
        /// URL of API
        /// </summary>
        private string _localhostUrl;

        /// <summary>
        /// Constructor with parameter
        /// </summary>
        /// <param name="localhostUlr"></param>
        public ClientService(string localhostUlr)
        {
            _client = new HttpClient();

            // Set Accept 
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            _localhostUrl = localhostUlr;
        }

        /// <summary>
        /// Request all vehicles from API
        /// </summary>
        /// <returns>All vehicles at parking</returns>
        public async Task<IList<VehicleDTO>> GetVehicles()
        {
            // Send request and get response
            HttpResponseMessage response = await _client.GetAsync($"{_localhostUrl}api/vehicles");

            // Check if reqsponse successful
            if (response.IsSuccessStatusCode)
            {
                // Read repsonse content
                string jsonString = await response.Content.ReadAsStringAsync();
                jsonString = jsonString.Substring(1, jsonString.Length - 2);

                jsonString = jsonString.Replace("\\", "");

                // Deserialize JSON string
                var vehicles = Deserialize<List<VehicleDTO>>(jsonString);
                return vehicles;
            }
            else
            {
                return new List<VehicleDTO>();
            }
        }

        /// <summary>
        /// Request vehicle by id from API
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Vehicle with certain id</returns>
        public async Task<VehicleDTO> GetVehicleById(string id)
        {
            // Send request and get response
            HttpResponseMessage response = await _client.GetAsync($"{_localhostUrl}api/vehicles/{id}");

            // Check response status code
            CheckResponse(response);

            // Read response content
            string jsonString = await response.Content.ReadAsStringAsync();

            // Deserialize JSON string
            var vehicle = Deserialize<VehicleDTO>(jsonString);
            return vehicle;
        }

        /// <summary>
        /// Request balance of parking from API
        /// </summary>
        /// <returns>Balance of parking</returns>
        public async Task<decimal> GetBalance()
        {
            // Send request and get response
            HttpResponseMessage response = await _client.GetAsync($"{_localhostUrl}api/parking/balance");

            // Read response content
            string jsonString = await response.Content.ReadAsStringAsync();

            var balance = Decimal.Parse(jsonString, CultureInfo.InvariantCulture);
            return balance;
        }

        /// <summary>
        /// Request capaciy of parking from API
        /// </summary>
        /// <returns>Capacity</returns>
        public async Task<int> GetCapacity()
        {
            // Send request and get response
            HttpResponseMessage response = await _client.GetAsync($"{_localhostUrl}api/parking/capacity");

            // Read response content
            string jsonString = await response.Content.ReadAsStringAsync();

            int capacity = Int32.Parse(jsonString);
            return capacity;
        }

        /// <summary>
        /// Request free places at parking from API
        /// </summary>
        /// <returns>Free places</returns>
        public async Task<int> GetFreePlaces()
        {
            // Send request and get response
            HttpResponseMessage response = await _client.GetAsync($"{_localhostUrl}api/parking/freePlaces");

            // Read response content
            string jsonString = await response.Content.ReadAsStringAsync();
            int freePlaces = Int32.Parse(jsonString);
            return freePlaces;
        }

        /// <summary>
        /// Send new vehicle to API and request to add it to parking
        /// </summary>
        /// <param name="vehicle"></param>
        public void AddVehicle(VehicleDTO vehicle)
        {
            // Create content for request
            HttpContent content = new StringContent(Serialize<VehicleDTO>(vehicle), Encoding.UTF8, "application/json");

            // Send request and get response
            HttpResponseMessage response = _client.PostAsync($"{_localhostUrl}api/vehicles", content).Result;

            // Check status code of response
            CheckResponse(response);
        }

        /// <summary>
        /// Request removing of vehicle
        /// </summary>
        /// <param name="id"></param>
        public void RemoveVehicle(string id)
        {
            // Send request and get response
            HttpResponseMessage response = _client.DeleteAsync($"{_localhostUrl}api/vehicles/{id}").Result;

            // Check status code of response
            CheckResponse(response);
        }

        /// <summary>
        /// Send request to top up vehicle's balance
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sum"></param>
        public void TopUpVehicle(string id, decimal sum)
        {
            var transaction = new TransactionInfoDTO() { VehicleId = id, Sum = sum };

            // Create content for request
            HttpContent content = new StringContent(JsonConvert.SerializeObject(transaction), Encoding.UTF8, "application/json");

            // Send request and get response
            HttpResponseMessage response = _client.PutAsync($"{_localhostUrl}api/transactions/topUpVehicle", content).Result;

            // Chekc response status code
            CheckResponse(response);
        }

        /// <summary>
        /// Request last parking transactions befor write to log
        /// </summary>
        /// <returns>Transactions for the last period</returns>
        public async Task<TransactionInfoDTO[]> GetLastParkingTransactions()
        {
            // Send request and get response
            HttpResponseMessage response = await _client.GetAsync($"{_localhostUrl}api/transactions/last");
            
            // Check repsonse status code
            CheckResponse(response);

            // Read response content
            string jsonString = await response.Content.ReadAsStringAsync();
            jsonString = jsonString.Substring(1, jsonString.Length - 2).Replace("\\", "");
            
            // Deserialize JSON string
            var transactions = Deserialize<TransactionInfoDTO[]>(jsonString);
            return transactions;
        }

        /// <summary>
        /// Request transactions from log file
        /// </summary>
        /// <returns>Transactions from log</returns>
        public async Task<string> GetLogTransactions()
        {
            // Send request and get response
            HttpResponseMessage response = await _client.GetAsync($"{_localhostUrl}api/transactions/all");

            // Check repsonse status code
            CheckResponse(response);

            // Read response content
            string transactions = await response.Content.ReadAsStringAsync();
            transactions = transactions.Replace(@"\n", "\n");

            return transactions;
        }

        /// <summary>
        /// Deserialize objects from JSON string to T type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <returns>Object of type T</returns>
        private T Deserialize<T>(string jsonString) where T : class
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        /// <summary>
        /// Serialize object of T type to JSON string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns>JSON string</returns>
        private string Serialize<T>(T obj) where T : class
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// Check status code of response from API
        /// </summary>
        /// <param name="response"></param>
        private void CheckResponse(HttpResponseMessage response)
        {
            // Check 'Not Found' and 'Bad Request'
            if (response.StatusCode == System.Net.HttpStatusCode.NotFound
                || response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new ArgumentException(response.Content.ReadAsStringAsync().Result
                    .Replace(@"\n", "\n").Trim('"'));
            }

            // Check if status code is successful
            response.EnsureSuccessStatusCode();
        }
    }
}

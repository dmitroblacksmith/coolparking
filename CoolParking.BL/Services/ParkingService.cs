﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    /// <summary>
    /// Service that provides main BL functions of parking
    /// </summary>
    public class ParkingService : IParkingService
    {
        /// <summary>
        /// Instance of Parking class
        /// </summary>
        Parking Parking { get; set; }

        /// <summary>
        /// Transaction for the last period before writing to log
        /// </summary>
        IList<TransactionInfo> _transactionInfos = new List<TransactionInfo>();

        /// <summary>
        /// Timer that determines period of money withdraw
        /// </summary>
        ITimerService _withdrawTimer;
        
        /// <summary>
        /// Timer that determines period of writing to log
        /// </summary>
        ITimerService _logTimer;

        /// <summary>
        /// Log service
        /// </summary>
        ILogService _logService;

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="withdrawTimer"></param>
        /// <param name="logTimer"></param>
        /// <param name="logService"></param>
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            // Assing services
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            // Set intervals for timers
            _logTimer.Interval = Settings.LogPeriod * 1000;
            _withdrawTimer.Interval = Settings.WithdrawPeriod * 1000;

            // Set event hadlers for timers
            _withdrawTimer.Elapsed += new ElapsedEventHandler(Withdraw);
            _logTimer.Elapsed += new ElapsedEventHandler(WriteToLog);

            // Start timers
            _withdrawTimer.Start();
            _logTimer.Start();
            
            // Create instance of Parking
            Parking = Parking.Source;

            Parking.Vehicles.Clear();
        }

        /// <summary>
        /// Withdraw money from vehicles' accounts at parking after ending of withdraw period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Withdraw(object sender, ElapsedEventArgs e)
        {
            foreach(var vehicle in Parking.Vehicles)
            {
                // Calculate withdraw sum for each vehicle
                var withdrawSum = Settings.Tariffs[vehicle.VehicleType] * (decimal)(vehicle.Balance <= 0 ? Settings.PenaltyRatio : 1.0);
                
                // Transfer money
                vehicle.Balance -= withdrawSum;
                Parking.Balance += withdrawSum;

                // Create new transaction info
                _transactionInfos.Add(new TransactionInfo(DateTime.Now, vehicle.Id, withdrawSum));
            }
        }

        /// <summary>
        /// Write to log after ending of log period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void WriteToLog(object sender, ElapsedEventArgs e)
        {
            string logStr = String.Empty;

            // Determine new transaction for the last interval of logging
            var transactions = _transactionInfos.ToList();

            // Concatenate all new transaction in string format for writing to log 
            foreach (var trans in transactions)
            {
                logStr += trans.ToString() + "\n";
                _transactionInfos.Remove(trans);
            }

            // Write to log
            _logService.Write(logStr);
        }

        /// <summary>
        /// Get balance of parking
        /// </summary>
        /// <returns>Parking balance</returns>
        public decimal GetBalance() 
        {
            return Parking.Balance;
        }

        /// <summary>
        /// Get general capacity of parking
        /// </summary>
        /// <returns>Available capacity of parking</returns>
        public int GetCapacity()
        {
            return Parking.Capacity;
        }

        /// <summary>
        /// Get number of free places at parking
        /// </summary>
        /// <returns>Free places at parking at the moment</returns>
        public int GetFreePlaces()
        {
            return Parking.Capacity - Parking.Vehicles.Count();
        }

        /// <summary>
        /// Get all vehicles at parking
        /// </summary>
        /// <returns>Vehicles at parking</returns>
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
        }

        /// <summary>
        /// Add new vehicle if there is at least one free place
        /// </summary>
        /// <param name="vehicle"></param>
        public void AddVehicle(Vehicle vehicle)
        {
            // Check if there is at least one free place
            if (Parking.Vehicles.Count() < Parking.Capacity)
            { 
                // Check if there is vehicle with the same id
                if (Parking.Vehicles.Where(v => v.Id == vehicle.Id).Any())
                {
                    throw new ArgumentException("Vehicle with the same 'Id' already exists!");
                }
                else
                {
                    // Add new vehicle to parking
                    Parking.Vehicles.Add(vehicle);
                }
            }
            else
            {
                throw new InvalidOperationException("There isn't any free place at parking!");
            }
        }

        /// <summary>
        /// Remove vehicle with no debt from parking 
        /// </summary>
        /// <param name="vehicleId"></param>
        public void RemoveVehicle(string vehicleId)
        {
            // Find vehicle by id
            var vehicle = FindVehicle(vehicleId);
            if (vehicle.Balance >= 0)
            {
                // Remove vehicle from parking
                Parking.Vehicles.Remove(vehicle);
            }
            else
            {
                throw new InvalidOperationException("It's prohibited to remove vehicle with debt!");
            }
        }

        /// <summary>
        /// Top up vehicle's balance
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="sum"></param>
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            // Check if top up sum is greater than 0
            if (sum > 0)
            {
                // Find vehicle by id
                var vehicle = FindVehicle(vehicleId);

                // Top up vehicle balance
                vehicle.Balance += sum;
            }
            else
            {
                throw new ArgumentException("Top up sum must be greater than 0!");
            }
        }

        /// <summary>
        /// Look for vehicle by id and check if it exists
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns>Vehicle</returns>
        Vehicle FindVehicle(string vehicleId)
        {
            // Check if id isn't empty
            if(vehicleId == String.Empty)
            {
                throw new ArgumentException("'Id' mustn't be empty!");
            }

            // Find vehicle by id or set null
            var vehicle = Parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle != null)
            {
                return vehicle;
            }
            else
            {
                throw new ArgumentException($"Vehicle with 'Id' equals '{vehicleId}' doesn't exist");
            }
        }

        /// <summary>
        /// Get parking transactions, which haven't been writen to log yet
        /// </summary>
        /// <returns>Transactions for the last time</returns>
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfos.ToArray();
        }

        /// <summary>
        /// Read all transaction from log file
        /// </summary>
        /// <returns>Info from log fime in String format</returns>
        public string ReadFromLog()
        {
            return _logService.Read();
        }

        /// <summary>
        /// Dispose resources
        /// </summary>
        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            Parking.Vehicles.Clear();
            Parking.Balance = 0;
        }
    }
}
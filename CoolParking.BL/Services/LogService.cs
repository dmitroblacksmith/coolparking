﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    /// <summary>
    /// Serbice to write and read information from log file
    /// </summary>
    public class LogService : ILogService
    {
        /// <summary>
        /// Path to log file
        /// </summary>
        public string LogPath { get; set; }
        
        /// <summary>
        /// Constructor with parameter
        /// </summary>
        /// <param name="logPath"></param>
        public LogService(string logPath)
        {
            this.LogPath = logPath;
        }

        /// <summary>
        /// Write line to log file
        /// </summary>
        /// <param name="str"></param>
        public void Write(string str) 
        {
            using(var file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(str);
            }
        }

        /// <summary>
        /// Read all lines from log file
        /// </summary>
        /// <returns>All info from log file in one string</returns>
        public string Read()
        {
            string logContent = String.Empty;

            // Check if log file exists
            if(!File.Exists(LogPath))
            {
                throw new InvalidOperationException("Log file wasn't found!");
            }
            else
            {
                logContent = File.ReadAllText(LogPath);
            }
            return logContent;
        }
    }
}
﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    /// <summary>
    /// Service to run timers for firing periodic events
    /// </summary>
    public class TimerService : ITimerService
    {
        /// <summary>
        /// Event that happens when timer finished work while one interval
        /// </summary>
        public event ElapsedEventHandler Elapsed;
        
        /// <summary>
        /// Period of time after which Elapsed event is raised
        /// </summary>
        public double Interval { get; set; }

        /// <summary>
        /// Timer to execute some methods at specific interval
        /// </summary>
        public Timer Timer { get; set; }

        /// <summary>
        /// Base contractor
        /// </summary>
        public TimerService() 
        {
            this.Timer = new Timer();

            // Subscribe handler for System.Timers.Timer Elapsed event that invokes Elapsed event of TimeService
            this.Timer.Elapsed += new ElapsedEventHandler(FireElapsedEvent);
            this.Timer.AutoReset = true;
        }

        /// <summary>
        /// Constructor with parameter
        /// </summary>
        /// <param name="interval"></param>
        public TimerService(double interval) : base()
        {
            // Convert interval from seconds to milliseconds
            this.Interval = interval * 1000;
        }

        /// <summary>
        /// Start timer
        /// </summary>
        public void Start()
        {
            this.Timer.Interval = Interval;
            this.Timer.Enabled = true;
        }

        /// <summary>
        /// Stop timer
        /// </summary>
        public void Stop()
        {
            this.Timer.Stop();
        }

        /// <summary>
        /// Dispose Timer
        /// </summary>
        public void Dispose()
        {
            this.Timer.Stop();
            this.Timer.Dispose();
        }

        /// <summary>
        /// Raises Elapsed event of TimeService
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FireElapsedEvent(object sender, ElapsedEventArgs e)
        {
            // Invoke Elapsed event of TimerService
            this.Elapsed?.Invoke(this, null);
        }
    }
}
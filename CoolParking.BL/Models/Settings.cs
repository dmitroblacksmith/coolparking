﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    /// <summary>
    /// Static class with settings of app
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Money on parking balance when it start work
        /// </summary>
        public static decimal StartBalance { get; set; } = 0;

        /// <summary>
        /// Available capacity of parking
        /// </summary>
        public static int Capacity { get; set; } = 10;

        /// <summary>
        /// Period in seconds that defines how often parking withdraw money 
        /// </summary>
        public static int WithdrawPeriod { get; set; } = 5;

        /// <summary>
        /// Period in seconds that defines how often parking write to log
        /// </summary>
        public static int LogPeriod { get; set; } = 60;

        /// <summary>
        /// Coefficient that increases tax in case of vehicle debt
        /// </summary>
        public static double PenaltyRatio { get; set; } = 2.5;

        /// <summary>
        /// Dictionary with tariffs for all vehicle types
        /// </summary>
        public static Dictionary<VehicleType, decimal> Tariffs { get; set; } = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.PassengerCar, 2.0M },
            { VehicleType.Bus, 3.5M },
            { VehicleType.Truck, 5.0M },
            { VehicleType.Motorcycle, 1.0M }
        };
    }
}
﻿using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        
        /// <summary>
        /// Id of vehicle which the fee was charged from
        /// </summary>
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        /// <summary>
        /// Fee that was charged from vehicle
        /// </summary>
        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        /// <summary>
        /// Date and time of transaction
        /// </summary>
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="transactionTime"></param>
        /// <param name="vehicleId"></param>
        /// <param name="sum"></param>
        public TransactionInfo(DateTime transactionDate, string vehicleId, decimal sum)
        {
            this.TransactionDate = transactionDate;
            this.VehicleId = vehicleId;
            this.Sum = sum;
        }

        /// <summary>
        /// Overrided version of ToString method
        /// </summary>
        /// <returns>Sting representation of transaction data</returns>
        public override string ToString()
        {
            return $"{TransactionDate}\t{VehicleId}\t{Sum}";
        }
    }
}
﻿namespace CoolParking.BL.Models
{
    /// <summary>
    /// Enumeration represents types of vehicle
    /// </summary>
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}
﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    /// <summary>
    /// Parking class represents transport pakring
    /// </summary>
    public sealed class Parking
    {
        /// <summary>
        /// Earned money
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Vehicles at parking
        /// </summary>
        public IList<Vehicle> Vehicles = new List<Vehicle>();
        
        /// <summary>
        /// Available capacity of parking
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Variable for single instance of Parking class
        /// </summary>
        private static Parking source = null;

        /// <summary>
        /// P{roperty for single instance of Parking class
        /// </summary>
        public static Parking Source
        {
            get
            {
                // Check if instance of PArking class is already created
                if (source == null)
                {
                    source = new Parking();
                }
                return source;
            }
        }

        /// <summary>
        /// Base constructor
        /// </summary>
        private Parking()
        {
            // Set available capacity from Settings
            Capacity = Settings.Capacity;
        }
    }
}
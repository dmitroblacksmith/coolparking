﻿using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    /// <summary>
    /// Vehicle class represent vehicle at parking
    /// </summary>
    public class Vehicle
    {
        /// <summary>
        /// Readonly unique identificator of vehicle
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; private set; }

        /// <summary>
        /// Readonly type of vehicle
        /// </summary>
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; private set; }

        /// <summary>
        /// Current money balance of vehicle
        /// </summary>
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        [JsonConstructor]
        private Vehicle() {}

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vehicleType"></param>
        /// <param name="balance"></param>
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            // Validation of id and balance values
            ValidateVehicle(id, (int)vehicleType, balance);
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        /// <summary>
        /// Generate randow Id for vehicle
        /// </summary>
        /// <returns>Randomly created vehicle id</returns>
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string alphabeticChars = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
            string numericChars = "0123456789";
            string randomNumber = String.Empty;
            for (int i = 0; i < 4; i++)
            {
                randomNumber += alphabeticChars[new Random().Next(1, 26) - 1];
            }
            randomNumber = randomNumber.Insert(2, "--");
            for(int i = 0; i < 4; i++)
            {
                randomNumber = randomNumber.Insert(3 + i, numericChars[new Random().Next(1, 10) - 1].ToString());
            }
            return randomNumber;
        }

        /// <summary>
        /// Validate id, vehicle type and balance values before assinging them to properties
        /// </summary>
        /// <param name="id"></param>
        /// <param name="balance"></param>
        void ValidateVehicle(string id, int type, decimal balance)
        {
            string exceptionMessage = String.Empty;

            // Check id format "AA-0000-AA" and if id isn't empty
            Regex rx = new Regex(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            if (id == string.Empty)
            {
                exceptionMessage += "'Id' mustn't be empty!\n";
            }
            else if (!rx.Match(id).Success)
            {
                exceptionMessage += "'Id' doesn't meet required format!\n";
            }

            // Check if balance is greater than 0
            if (balance <= 0)
            {
                exceptionMessage += "'Balance' must be greater than 0!\n";
            }

            // Check type of vehicle
            if (type < 0 || type > 3)
            {
                exceptionMessage += "'Type' is not correct!\n";
            }

            // Throw exception if validation was failed
            if (exceptionMessage != String.Empty)
            {
                throw new ArgumentException(exceptionMessage);
            }
        }
    }
}
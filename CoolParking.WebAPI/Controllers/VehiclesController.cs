﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTO;

namespace CoolParking.WebAPI.Controllers
{
    /// <summary>
    /// Controller for executing operations with vehicles
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        // Instance of service that represents parking functionality
        IParkingService _parkingService;

        /// <summary>
        /// Constructor with DI
        /// </summary>
        /// <param name="parkingService"></param>
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        /// <summary>
        /// GET api/vehicles
        /// </summary>
        /// <returns>All vehicles at parking</returns>
        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> GetVehicles()
        {
            var vehicles = _parkingService.GetVehicles();
            return Ok(JsonConvert.SerializeObject(vehicles));
        }

        /// <summary>
        /// GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Vehicle with certain id</returns>
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            // Check format of id
            if (!CheckId(id))
            {
                return BadRequest("Incorrect type of Id");
            }

            // Check if vehicle with certain id exists
            var vehicle = _parkingService.GetVehicles().Where(v => v.Id == id).FirstOrDefault();
            if (vehicle == null)
            {
                return NotFound($"There's no vehicle with Id '{id}' at parking");
            }
            else
            {
                return Ok(JsonConvert.SerializeObject(vehicle));
            }
        }

        /// <summary>
        /// POST api/vehicles
        /// </summary>
        /// <param name="vehicleDto"></param>
        /// <returns>Vehicle that was added to parking</returns>
        [HttpPost]
        public ActionResult<Vehicle> PostVehicle(VehicleDTO vehicleDto)
        {
            try
            {
                Vehicle vehicle = new Vehicle(vehicleDto.Id, vehicleDto.VehicleType, vehicleDto.Balance);
                
                // Add new vehicle to parking
                _parkingService.AddVehicle(vehicle);
                return CreatedAtAction(nameof(PostVehicle), JsonConvert.SerializeObject(vehicle));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Result of operation</returns>
        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            // Ckech id format
            if (!CheckId(id))
            {
                return BadRequest("Incorrect type of Id");
            }

            try
            {
                // Remove vehicle from parking
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Check if id format is "AA-0000-AA"
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Check result</returns>
        bool CheckId(string id)
        {
            if (!new Regex(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}").IsMatch(id))
            {
                return false;
            }

            return true;
        }
    }
}
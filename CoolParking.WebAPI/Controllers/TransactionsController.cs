﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    /// <summary>
    /// Controller for executing operations with transactions
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        // Instance of service that represents parking functionality
        IParkingService _parkingService;

        /// <summary>
        /// Constructor with DI
        /// </summary>
        /// <param name="parkingService"></param>
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        /// <summary>
        /// GET api/transactions/last
        /// </summary>
        /// <returns>Lat transactions befor write to log</returns>
        [HttpGet("last")]
        public ActionResult<TransactionInfo[]> GetLast()
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(_parkingService.GetLastParkingTransactions()));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// GET api/transactions/all
        /// </summary>
        /// <returns>All transactions from log file</returns>
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                string allTransactions = String.Empty;

                // Read all transactions from log
                string[] logTransactions = _parkingService.ReadFromLog().Split('\n');

                // Format transaction data from log file
                foreach (var transaction in logTransactions)
                {
                    if (transaction != "\n" && transaction != "\r" && transaction != String.Empty)
                    {
                        var transactionArray = transaction.Split('\t');
                        allTransactions += FormatTransaction(transactionArray[1], transactionArray[0], 
                            transactionArray[2]);
                    }
                }

                return Ok(allTransactions);
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }

        // Make transaction more user friendly and more readable 
        string FormatTransaction(string vehicleId, string date, string sum)
        {
            return $"{date}: {sum} money withdrawn from vehicle with Id='{vehicleId}'.\n";
        }

        /// <summary>
        /// PUT api/transactions/topUpVehicle
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns>Vehicle that was topped up</returns>
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle(TransactionInfo transaction)
        {
            try
            {
                // Check if vehicle with certain id exists
                var vehicle = _parkingService.GetVehicles().Where(v => v.Id == transaction.VehicleId).FirstOrDefault();
                if (vehicle == null)
                {
                    return NotFound();
                }
                else
                {
                    // Top up vehicle's balance
                    _parkingService.TopUpVehicle(transaction.VehicleId, transaction.Sum);
                    return Ok(JsonConvert.SerializeObject(vehicle));
                }

            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
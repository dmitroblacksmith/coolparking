﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    /// <summary>
    /// Controller for executing parking functionality
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        // Instance of service that represents parking functionality
        IParkingService _parkingService;

        /// <summary>
        /// Constructor with DI
        /// </summary>
        /// <param name="parkingService"></param>
        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        /// <summary>
        /// GET api/parking/balance
        /// </summary>
        /// <returns>PArking balance</returns>
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        /// <summary>
        /// GET api/parking/capacity
        /// </summary>
        /// <returns>Parking capacity</returns>
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        /// <summary>
        /// GET api/parking/freePlaces
        /// </summary>
        /// <returns>Free places at parking</returns>
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}
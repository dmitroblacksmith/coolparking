﻿using Newtonsoft.Json;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.DTO
{
    /// <summary>
    /// DTO entity for transfering vehicle data
    /// </summary>
    public class VehicleDTO
    {
        /// <summary>
        /// Vehicle id
        /// </summary>
        [JsonProperty]
        public string Id { get; set; }

        /// <summary>
        /// Vehicle type
        /// </summary>
        [JsonProperty]
        public VehicleType VehicleType { get; set; }

        /// <summary>
        /// Balance of vehicle
        /// </summary>
        [JsonProperty]
        public decimal Balance { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        [JsonConstructor]
        public VehicleDTO() { }
    }
}

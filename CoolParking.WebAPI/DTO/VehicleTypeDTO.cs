﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.DTO
{
    /// <summary>
    /// DTO enum for transfering vehicle type
    /// </summary>
    public enum VehicleTypeDTO
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}

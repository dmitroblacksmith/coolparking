using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        // Path to log file
        readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Define implementation of service interfaces
            services.AddTransient<ITimerService, TimerService>();
            services.AddTransient<ILogService>(s => new LogService(_logFilePath));
            
            // Define implementation for IParkingService and start parking
            services.AddSingleton<IParkingService>(p => new ParkingService(p.GetRequiredService<ITimerService>(),
                p.GetRequiredService<ITimerService>(), p.GetRequiredService<ILogService>()));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
